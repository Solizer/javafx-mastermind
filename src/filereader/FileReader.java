package filereader;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class FileReader implements FileReaderInterface
{
	private List<Score> scores;
	private static Path FILENAME = Path.of("playdata.txt");

	public FileReader()
	{
		scores = new LinkedList<>();
		if(!Files.exists(FILENAME))
		{
			try
			{
				Files.createFile(FILENAME);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		try
		{
			scores = Files.lines(FILENAME).map(x -> Score.toScore(x)).collect(Collectors.toList());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		Collections.sort(scores);
	}

	@Override
	public List<Score> getScores()
	{
		try
		{
			scores = Files.lines(FILENAME).map(x -> Score.toScore(x)).collect(Collectors.toList());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		Collections.sort(scores);
		return scores;
	}

	@Override
	public void addScore(int score)
	{
		scores.add(new Score(score));
		Collections.sort(scores);
		try
		{
			FileWriter writer = new FileWriter(FILENAME.toString());
			for(Score x: scores)
			{
				writer.write(x.toCSV());
				writer.write("\n");
			}
			writer.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public void addScore(int score, LocalDateTime dateTime)
	{
		scores.add(new Score(score, dateTime));
		Collections.sort(scores);
		try
		{
			FileWriter writer = new FileWriter(FILENAME.toString());
			for(Score x: scores)
			{
				writer.write(x.toCSV());
				writer.write("\n");
			}
			writer.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		FileReader r = new FileReader();
		r.addScore(10);
		r.addScore(20, LocalDateTime.of(2019, 8, 8, 10, 22));
		r.addScore(30, LocalDateTime.of(2018, 8, 8, 10, 22));
		List<Score> l = r.getScores();
		l.forEach(x -> System.out.println(x.toString()));
	}
}
