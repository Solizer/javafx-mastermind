package filereader;

import java.time.LocalDateTime;
import java.util.List;

public interface FileReaderInterface
{
	public List<Score> getScores();

	public void addScore(int score);

	public void addScore(int score, LocalDateTime dateTime);
}
