package filereader;

import java.time.LocalDateTime;

public class Score implements Comparable<Score>
{
	private int score;
	private LocalDateTime dateTime;

	public Score(int score)
	{
		this.score = 14 - score;
		this.dateTime = LocalDateTime.now();
	}

	public Score(int score, LocalDateTime dateTime)
	{
		this.score = score;
		this.dateTime = dateTime;
	}

	public String toCSV()
	{
		return score + ";" + dateTime.toString();
	}

	public String toString()
	{
		StringBuilder st = new StringBuilder();
		st.append((dateTime.getDayOfMonth() < 10) ? "0" : "");
		st.append(dateTime.getDayOfMonth());
		st.append(". ");
		st.append(dateTime.getMonth().toString());
		st.append(" ");
		st.append(dateTime.getYear());
		st.append(" ");
		st.append(dateTime.getHour());
		st.append(":");
		if(Integer.toString(dateTime.getMinute()).length() <= 1)
		{
			st.append(0);
		}
		st.append(dateTime.getMinute());
		st.append(":\t\t");
		st.append((score < 10) ? "  " : "");
		st.append(score);
		st.append((score == 1) ? " point" : " points");
		return st.toString();
	}

	public static Score toScore(String string)
	{
		String[] split = string.split(";");
		return new Score(Integer.parseInt(split[0]), LocalDateTime.parse(split[1]));
	}

	@Override
	public int compareTo(Score o)
	{
		return o.dateTime.compareTo(this.dateTime);
	}
}
