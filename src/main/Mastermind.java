package main;

import filereader.FileReader;
import filereader.FileReaderInterface;
import filereader.Score;
import gameview.GameView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.*;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.time.LocalDateTime;
import java.util.List;

public class Mastermind extends Application
{
	private Stage primaryStage;
	private FileReaderInterface fileReader;
	private GameView view;
	private boolean easy;

	public static void startApp(String[] args)
	{
		launch(args);
	}

	public void start(Stage primaryStage)
	{
		this.primaryStage = primaryStage;
		primaryStage.initStyle(StageStyle.UNDECORATED);
		this.fileReader = new FileReader();
		this.view = new GameView(this);
		this.easy = false;
		primaryStage.setScene(createMainMenu());
		primaryStage.show();
	}

	private Scene createMainMenu()
	{
		Text heading = new Text("Mastermind");
		heading.setStyle(Commons.HEADING);
		heading.setFill(Color.MEDIUMSEAGREEN);
		heading.setFont(Font.font("Verdana"));

		Button[] mainMenuButtons = new Button[4];

		mainMenuButtons[0] = new Button("Start Game");
		mainMenuButtons[1] = new Button("Settings");
		mainMenuButtons[2] = new Button("Scores");
		mainMenuButtons[3] = new Button("Exit");
		for(Button x: mainMenuButtons)
		{
			x.setStyle(Commons.STYLE);
			x.setFont(new Font("Verdana", 40));
			x.setOnMouseEntered(y -> x.setEffect(new ColorAdjust(0, 0, 0.19, 0.2)));
			x.setOnMouseExited(y -> x.setEffect(null));
		}
		mainMenuButtons[0].setOnAction(x -> primaryStage.setScene(createGame()));
		mainMenuButtons[1].setOnAction(x -> primaryStage.setScene(createSettings()));
		mainMenuButtons[2].setOnAction(x -> primaryStage.setScene(createScores()));
		mainMenuButtons[3].setOnAction(x -> Platform.exit());

		VBox mainMenuBox = new VBox(mainMenuButtons);
		mainMenuBox.setAlignment(Pos.CENTER);
		mainMenuBox.setSpacing(5);
		mainMenuBox.setBackground(Background.EMPTY);
		VBox headingBox = new VBox(heading);
		headingBox.setAlignment(Pos.CENTER);
		headingBox.setPadding(new Insets(30, 0, 0, 0));
		headingBox.setBackground(Background.EMPTY);

		BorderPane mainMenuPane = new BorderPane();
		mainMenuPane.setTop(headingBox);
		mainMenuPane.setCenter(mainMenuBox);
		BorderPane.setAlignment(mainMenuBox, Pos.CENTER);
		mainMenuPane.setStyle("-fx-background-color:transparent");

		Scene mainMenuScene = new Scene(mainMenuPane, Commons.WIDTH, Commons.HEIGHT);
		mainMenuScene.setFill(Color.rgb(25, 25, 26));
		return mainMenuScene;
	}

	private Scene createGame()
	{
		return view.getGame(easy);
	}

	private Scene createSettings()
	{
		BorderPane borderPane = new BorderPane();
		borderPane.setStyle("-fx-background-color:transparent");
		Button easy = new Button("Easy Mode");
		easy.setOnAction(x -> this.easy = true);
		easy.setOnMouseEntered(y -> easy.setEffect(new ColorAdjust(0, 0, 0.19, 0.2)));
		easy.setOnMouseExited(y -> easy.setEffect(null));
		easy.setStyle(Commons.STYLE);
		Button hard = new Button("Hard Mode");
		hard.setOnAction(x -> this.easy = false);
		hard.setOnMouseEntered(y -> hard.setEffect(new ColorAdjust(0, 0, 0.19, 0.2)));
		hard.setOnMouseExited(y -> hard.setEffect(null));
		hard.setStyle(Commons.STYLE);
		Button returnButton = new Button("Return to Main Menu");
		returnButton.setOnAction(x -> primaryStage.setScene(createMainMenu()));
		returnButton.setOnMouseEntered(y -> returnButton.setEffect(new ColorAdjust(0, 0, 0.19, 0.2)));
		returnButton.setOnMouseExited(y -> returnButton.setEffect(null));
		returnButton.setStyle(Commons.STYLE);
		VBox settingsBox = new VBox();
		settingsBox.getChildren().add(easy);
		settingsBox.getChildren().add(hard);
		borderPane.setBottom(returnButton);
		borderPane.setAlignment(returnButton, Pos.CENTER);
		borderPane.setCenter(settingsBox);
		borderPane.setAlignment(settingsBox, Pos.CENTER);
		settingsBox.setAlignment(Pos.CENTER);
		settingsBox.setStyle("-fx-background-color:transparent");
		Scene settingsScene = new Scene(borderPane, Commons.WIDTH, Commons.HEIGHT);
		settingsScene.setFill(Color.rgb(25, 25, 26));
		return settingsScene;
	}

	private Scene createScores()
	{
		List<Score> scores = fileReader.getScores();
		BorderPane scoresBox = new BorderPane();
		scoresBox.setStyle("-fx-background-color:rgb(25, 25, 26)");
		VBox entryBox = new VBox();
		entryBox.setSpacing(10);
		entryBox.setPadding(new Insets(0, 0, 0, 20));
		entryBox.setStyle("-fx-background-color:rgb(25, 25, 26)");
		entryBox.setAlignment(Pos.CENTER);
		if(scores.isEmpty())
		{
			Text noScores = new Text("Sorry, no scores available!");
			noScores.setStyle(Commons.SCORE);
			noScores.setFill(Color.WHITE);
			entryBox.getChildren().add(noScores);
		}
		else
		{
			for(int i = 17; i >= 0; i--)
			{
				try
				{
					Text text = new Text(scores.get(i).toString());
					text.setStyle(Commons.SCORE);
					text.setFill(Color.WHITE);
					entryBox.getChildren().add(text);
				}
				catch(IndexOutOfBoundsException e)
				{

				}
			}
		}
		/*Text text = new Text(" ");
		text.setStyle(Commons.STYLE);
		text.setFill(Color.WHITE);*/
		Button returnButton = new Button("Return to Main Menu");
		returnButton.setOnAction(x -> primaryStage.setScene(createMainMenu()));
		returnButton.setOnMouseEntered(y -> returnButton.setEffect(new ColorAdjust(0, 0, 0.19, 0.2)));
		returnButton.setOnMouseExited(y -> returnButton.setEffect(null));
		returnButton.setStyle(Commons.STYLE);
		scoresBox.setBottom(returnButton);
		scoresBox.setAlignment(returnButton, Pos.CENTER);
		scoresBox.setCenter(entryBox);
		scoresBox.setAlignment(entryBox, Pos.CENTER);

		Scene scoreScene = new Scene(scoresBox, Commons.WIDTH, Commons.HEIGHT);
		scoreScene.setFill(Color.rgb(25, 25, 26));
		return scoreScene;
	}

	public void endGame()
	{
		primaryStage.setScene(createMainMenu());
	}

	public Stage getPrimaryStage()
	{
		return primaryStage;
	}

	public void saveScore(int score)
	{
		fileReader.addScore(score);
	}
}
